# Transfer Module

This is the transfer module. The entry point is `/src/index.js`.

## Starting up

Install dependencies:

    npm install

Run this widget:

    npm start

This will start a web server on [http://localhost:3001](http://localhost:3001)
