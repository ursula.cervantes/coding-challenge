let transfer
beforeEach(() => {
    jest.resetModules()
    transfer = require('@api').transfer
});

test('Call transfer button', () => {
    // Set up our document body
    document.body.innerHTML = '<div><section></section></div>'

    // This module has a side-effect
    require('./index.js')

    document.querySelector('#transfer-money-button').click()
    expect(transfer.mock.calls[0][0]).toEqual({
        'pin': '0852',
        'amount': 26.0,
        'partnerIban': 'DE04100110012629339457',
        'partnerName': 'Valentin Stalf',
        'referenceText': 'Gift',
    })
})
