// This file won't be shared with the candidate.
// The purpose is to test the candidate's solution with this test suite

const loadTransferModule = () => {
    // Set up our document body
    document.body.innerHTML = '<div><section></section></div>'

    // This module has a side-effect
    require('./index.js')
}

const getElementVisibility = (element) => window.getComputedStyle(element).visibility
const getElementDisplay = (element) => window.getComputedStyle(element).display
const isElementHidden = (element) => getElementVisibility(element) === 'hidden' && getElementDisplay(element) === 'none'

const fillFirstStepData = () => { 
    document.querySelector('#partner-iban').value = 'DE04100110012629339457'
    document.querySelector('#partner-name').value = 'Valentin Stalf'
}
const fillSecondStepData = () => {
    document.querySelector('#amount').value = '26.0'
    document.querySelector('#reference-message').value = 'Gift'
 }
const fillThirdStepData = () => {
    document.querySelector('#pin').value = '0852'
}

describe('First Step - Add Partner`s name and IBAN', () => {

    beforeEach(() => {
        loadTransferModule()
    })

    test('Fill data and move to next step', () => {
        const firstStep = document.querySelector('#first-step-section')
        const secondStep = document.querySelector('#second-step-section')
        const thirdStep = document.querySelector('#third-step-section')
        const backButton = document.querySelector('#go-back-button')
        expect(isElementHidden(firstStep)).toEqual(false)
        expect(isElementHidden(secondStep)).toEqual(true)
        expect(isElementHidden(thirdStep)).toEqual(true)
        expect(isElementHidden(backButton)).toEqual(true)

        expect(document.querySelector('#continue-first-step-button').disabled).toEqual(true)
        fillFirstStepData()
        expect(document.querySelector('#continue-first-step-button').disabled).toEqual(false)

        document.querySelector('#continue-first-step-button').click()
    })

    test('Fill step with invalid data', () => {
        document.querySelector('#partner-iban').value = '04100110012'
        expect(document.querySelector('#continue-first-step-button').disabled).toEqual(true)

        document.querySelector('#partner-name').value = 'Valentin Stalf'
        expect(document.querySelector('#continue-first-step-button').disabled).toEqual(true)
    })
})

describe('Second Step - Add amount and reference number', () => {

    beforeEach(() => {
        loadTransferModule()
    })

    test('Fill data and move to next step', () => {
        const firstStep = document.querySelector('#first-step-section')
        const secondStep = document.querySelector('#second-step-section')
        const thirdStep = document.querySelector('#third-step-section')
        const backButton = document.querySelector('#go-back-button')
        expect(isElementHidden(firstStep)).toEqual(true)
        expect(isElementHidden(secondStep)).toEqual(false)
        expect(isElementHidden(thirdStep)).toEqual(true)
        expect(isElementHidden(backButton)).toEqual(false)

        expect(document.querySelector('#continue-second-step-button').disabled).toEqual(true)

        fillSecondStepData()
        expect(document.querySelector('#continue-second-step-button').disabled).toEqual(false)

        document.querySelector('#continue-second-step-button').click()
    })

    test('Fill step with invalid data', () => {
        document.querySelector('#amount').value = 'abc'
        expect(document.querySelector('#continue-second-step-button').disabled).toEqual(true)

        // Fill with more than 135 characters
        document.querySelector('#reference-message').value = 'abcaskldfwSLCMNASDJKF AKDSFCJ AS FASF AHUFH AKDJFH aidfjkhasd fuiwa asuief ksjdfhausiefyaw efnasdj f238947ruifjasf aiwueyr8923qrwiofjasdhfas y987r3uqwa'
        expect(document.querySelector('#continue-second-step-button').disabled).toEqual(true)
    })

    test('Fill data, go back and forth', () => {
        fillFirstStepData()
        document.querySelector('#continue-first-step-button').click()
        fillSecondStepData()

        document.querySelector('#go-back-button').click()
        const firstStep = document.querySelector('#first-step-section')
        expect(isElementHidden(firstStep)).toEqual(false)
        
        document.querySelector('#continue-first-step-button').click()
        const secondStep = document.querySelector('#second-step-section')
        expect(isElementHidden(secondStep)).toEqual(false)
        document.querySelector('#continue-second-step-button').click()
    })
})

describe('Third Step - Transfer money', () => {
    let transfer

    beforeEach(() => {
        loadTransferModule()
        jest.resetModules()
        transfer = require('@api').transfer
    })

    test('Fill data and move to next step', () => {
        const firstStep = document.querySelector('#first-step-section')
        const secondStep = document.querySelector('#second-step-section')
        const thirdStep = document.querySelector('#third-step-section')
        const backButton = document.querySelector('#go-back-button')
        expect(isElementHidden(firstStep)).toEqual(true)
        expect(isElementHidden(secondStep)).toEqual(true)
        expect(isElementHidden(thirdStep)).toEqual(false)
        expect(isElementHidden(backButton)).toEqual(false)

        expect(document.querySelector('#transfer-money-button').disabled).toEqual(true)

        fillThirdStepData()
        expect(document.querySelector('#transfer-money-button').disabled).toEqual(false)

        document.querySelector('#transfer-money-button').click()

        expect(transfer.mock.calls[0][0]).toEqual({
            'pin': '0852',
            'amount': 26.0,
            'partnerIban': 'DE04100110012629339457',
            'partnerName': 'Valentin Stalf',
            'referenceText': 'Gift',
        })
    })

    test('Fill data, go back and forth', () => {
        fillFirstStepData()
        document.querySelector('#continue-first-step-button').click()
        fillSecondStepData()
        document.querySelector('#continue-second-step-button').click()
        fillThirdStepData()

        document.querySelector('#go-back-button').click()
        document.querySelector('#go-back-button').click()

        document.querySelector('#continue-first-step-button').click()
        document.querySelector('#continue-second-step-button').click()

        document.querySelector('#transfer-money-button').click()

    })
})