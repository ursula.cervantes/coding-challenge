
import styles from './index.css'
import * as api from '@api'

(function () {
    document.querySelector(
        'section'
    ).innerHTML += `<div class=${styles.container}><button id="transfer-money-button">Transfer money</button></div>`

    document.querySelector('#transfer-money-button').addEventListener('click', () => {
        console.log('Implement transfer money flow here')
        api.transfer({
            'pin': '0852',
            'amount': 26.0,
            'partnerIban': 'DE04100110012629339457',
            'partnerName': 'Valentin Stalf',
            'referenceText': 'Gift',
        })
    })
})()
