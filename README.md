# Welcome to the Coding Challenge

This **README** file contains the instructions you will need to complete your task in the form of a User Story – the format regularly used by N26 engineers in our weekly sprints.

#### Instructions

- This master branch already contains code which you will use for your task. Please add any new code to a new feature branch. When you’re done with the task, please submit a GitLab merge request to master.

- For the parts of the task which you feel are unclear, we encourage you to make your own assumptions as long as they are documented in your README file. However, if you have a question or concern that is blocking you from completing the work, please reach out to us via email.

- Run `npm install` and `npm start` for both banking and transferModule. This will start a web server on [http://localhost:3001](http://localhost:3001) where you can see the final result.

---

### User Story

As a N26 user, I want to be able to send money to another bank.
For this, we need 3 steps that are described below.


#### Design Prototype

First Step
![Send money form](./send-money.jpg)

Second Step
![Transfer money form](./transfer-form.jpg)

Third Step
![Confirm transfer](./confirm-transfer.jpg)

#### Acceptance Criteria

- Insert a valid Name and IBAN number
- Insert a valid amount of money to transfer (at least 0.01 Euro but no more than available amount in the main account)
- The user can add a reference number or message. Maximun characters: 135
- Enter confirmation PIN to authorize the transaction.
- The user must be able to go back and forth between the steps without losing the data previously entered.

- Use the following ids for the inputs and buttons
    - partner-iban
    - partner-name
    - continue-first-step-button
    - go-back-button
    - amount
    - reference-message
    - continue-second-step-button
    - pin
    - transfer-money-button

- Use the following ids for every step based on the prototype
    - first-step-section
    - second-step-section
    - third-step-section

#### Important Notes

- **Use native web components.**
- The Acceptance Criteria is the source of truth, not the design. 
- There is a supplied design that shows the intended experience / visuals for the widget itself - the rest is up to you.
- You should not use any third party library.
- For this boilerplate to run, you will need to run `npm install; npm start` and make sure that the 3000 port is available.

---

### Good vibes! ✨
