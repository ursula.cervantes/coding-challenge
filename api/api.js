/**
pin: String
amount: InputAmount
partnerIban: String
partnerName: String
referenceText: String
 */
const transferValidArguments = ['pin', 'amount', 'partnerIban', 'partnerName', 'referenceText']

const check = (obj, arr) => Object.keys(obj).every(e => arr.includes(e))

const validator = (args) => {
    if (!check(args, transferValidArguments)) {
        console.error('Expected argument to be a valid Object, got: ', args)
        return;
    }
    transferValidArguments.forEach((type) => {
        if (!args[type] && type !== 'referenceText') {
            console.error(`Missing ${type} argument`)
            throw new Error('We cannot create a transfer with this information.')
        }
    });

    if(args.pin !== '0852'){
        throw new Error('The PIN is incorrect')
    }
};

let transfer
if (process.env.NODE_ENV === 'test') {
    transfer = jest.fn(validator)
} else {
    transfer = (args) => {
        validator(args)
        console.table(args)
    };
}

module.exports = {
    validator,
    transfer,
};
