# Banking Website

This is a rough strip of the [https://app.n26.com](https://app.n26.com) website.

The `index.html` has the transfer-module script added at the end of the `section` tag.

    <script src="http://localhost:3001/index.js"></script>


## Starting up

Install dependencies:

    npm install

Run this website:

    npm start

This will start a web server on [http://localhost:3000](http://localhost:3000)
